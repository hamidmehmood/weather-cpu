import { weatherBitUrl, weatherBitKey} from "../shared/constants";

export async function getAmbientTemperature() {

    const {data} = await weatherApiCall();
    const {[0]: {temp}} = data;
    return temp;
}

function weatherApiCall() {
    const requestString = `?key=${weatherKey}&lang=en&units=M&lat=53.944&lon=-1.088`;

    const request = new Request(`${weatherUrl}${requestString}`);
    return fetch(request)
        .then((response) => response.json());
}
